const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Table
const userSchema = new Schema(
  {
    firstname: {
      type: String,
      required: [true, "Firstname is required"],
      minlength: 3,
      maxlength: 255,
    },
    lastname: {
      type: String,
      required: [true, "Lastname is required"],
      minlength: 3,
      maxlength: 255,
    },
    email: {
      type: String,
      required: [true, "Email is required"],
      minlength: 3,
      maxlength: 255,
    },
    phoneNumber: {
      type: String,
      default: null,
      minlength: 3,
      maxlength: 255,
    },
    password: {
      type: String,
      required: [true, "Password is required"],
    },
  },
  {
    collection: "users",
  }
);

//Export modules
module.exports = mongoose.model("users", userSchema);
